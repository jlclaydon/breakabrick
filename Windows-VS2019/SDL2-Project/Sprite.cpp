#include "Sprite.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "AABB.h"

#include <stdexcept>

/**
 * Sprite
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Sprite::Sprite()
{
    speed = 50.0f;

    //Postion
    position = nullptr;

    //Velocity
    velocity = nullptr;

    // Set texture pointer
    texture = nullptr;
    
    // Animation array and size
    animations = nullptr;
    maxAnimations = 0;
    
    // Bounding box - can only 
    // be setup in derived class
    aabb = nullptr;
}

/**
 * initSprite
 * 
 * Function to populate an sprite structure from given paramters. 
 * 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @param maxAnimations total number of animations 
 * @param texturePath path to texture
 * @param position - initial position. 
 * @exception Throws an exception on file not found or out of memory. 
 */
void Sprite::init(int maxAnimationTypes, bool linearAnimation, Vector2f*  initPos)
{
    //setup max animations
    this->maxAnimations = maxAnimationTypes;

    // Allocate position and velocity - set values
    position = new Vector2f(initPos);
    velocity = new Vector2f();
    velocity->zero();


    /*
     
    // Create player texture from file, optimised for renderer
    texture = createTextureFromFile(texturePath.c_str(), renderer);

    if (texture == nullptr)
        throw std::runtime_error("File not found!");
       
    */


    // create array of pointers to animations. 
    // child classes will have to init these!
    animations = new Animation*[maxAnimations];

    // Allocate memory for the animation structures
    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i] = new Animation();
        animations[i]->setLinearAnimation(linearAnimation);
    }

}

/**
 * ~Sprite
 * 
 * Destroys the Sprite and any associated 
 * objects 
 * 
 */
Sprite::~Sprite()
{
    // Deallocate memory
    // set pointers back to null (stale pointer).
    delete position;
    position = nullptr;

    delete velocity;
    velocity = nullptr;

    // Clean up animations - free memory
    for (int i = 0; i < maxAnimations; i++)
    {
        // Clean up the animaton structure
        // allocated with new so use delete.
        delete animations[i];
        animations[i] = nullptr;
    }

    delete [] animations;
    animations = nullptr;

    delete aabb;
    aabb = nullptr;

    // Clean up
    SDL_DestroyTexture(texture);
    texture = nullptr;
}

/**
 * draw
 * 
 * Method draw a Player object
 * 
 * @param renderer SDL_Renderer to draw to
 */
void Sprite::draw(SDL_Renderer *renderer)
{
   

    // Get current animation based on the state.
    Animation *current = this->animations[getCurrentAnimationState()];

 //   SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle); // draw sprite

    /*
    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255); //yellow
    SDL_Rect bar;
    bar.x = targetRectangle.x; 
    bar.y = targetRectangle.y;
    bar.h = 25;
    bar.w = 100;
    SDL_RenderFillRect(renderer, &bar); // draw colour rect
    */
}

/**
 * update
 * 
 * Method to update the Sprite 
 * 
 * @param timeDeltaInSeconds the time delta in seconds
 */
void Sprite::update(float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    Vector2f movement(velocity);
    movement.scale(timeDeltaInSeconds);

    // Update player position.
    position->add(&movement);

    // Move sprite to nearest pixel location.
    //targetRectangle.x = round(position->getX());
    //targetRectangle.y = round(position->getY());

    animations[getCurrentAnimationState()]->update(timeDeltaInSeconds);

    // Get current animation
    Animation *current = animations[getCurrentAnimationState()];

    // let animation update itself.
    current->update(timeDeltaInSeconds);

    // Move bounding box
    if(aabb != nullptr)
        aabb->setPosition(position);

    srcRect = animations[getCurrentAnimationState()]->getCurrentFrame();
    
}

 Vector2f* Sprite::getPosition()
 {
     return position;
 }

void Sprite::setPosition(Vector2f* pos)
{
    position->setX(pos->getX());
    position->setY(pos->getY());
}

Vector2f* Sprite::getVelocity()
{
    return velocity;
}

void Sprite::setVelocity(Vector2f* velocity)
{
    this->velocity = velocity;
}

 AABB* Sprite::getAABB()
 {
     return aabb;
 }

 //The player and npcs require different offsets so these are set in their respective init() methods
//sets the offset of x for sprite reposition
 void Sprite::setOffsetX(int offsetX)
 {
     this->offsetX = offsetX;
 }
 //sets the offset of y for sprite reposition 
 void Sprite::setOffsetY(int offsetY)
 {
     this->offsetY = offsetY;
 }

 SDL_Rect* Sprite::getSrcRect()
 {
     return srcRect;
 }

 SDL_Rect* Sprite::getTarRect()
 {
     return tarRect;
 }