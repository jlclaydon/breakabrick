#ifndef SIZE_DOWN_H_
#define SIZE_DOWN_H_

#include "PowerUp.h"

class PowerUp_SizeDown : public PowerUp
{
protected:

public:
	PowerUp_SizeDown();
	~PowerUp_SizeDown();

	void performPowerUp();

	void init(int windowWidth, int windowHeight, Player* player, Game* game);
};

#endif