#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "TextureUtils.h"

class TimeUtils;


class AABB;
class Breakabrick;
class MenuButtonLargeText;
class ScrollingBackground;
class UserInput;

class MainMenu
{
private:

	Breakabrick* breakabrick;
	SDL_Renderer* gameRenderer;
	TimeUtils* timeUtils;
	UserInput* userInput;

	SDL_Texture* backgroundTexture;
	ScrollingBackground* scrollingBackground;

	MenuButtonLargeText* button;

	int mouseX;
	int mouseY;

	bool quit = false;

	AABB* mouseAABB();

public:

	MainMenu();
	~MainMenu();

	void init(Breakabrick* breakabrick, SDL_Renderer* gameRenderer, TimeUtils* timeUtils, UserInput* userInput);

	void run();

	void setQuit(bool quit);

	void drawBackground();

	
};

#endif

