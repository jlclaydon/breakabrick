#ifndef SPRITE_H_
#define SPRITE_H_

#include "SDL2Common.h"

#include <string>
using std::string;

// Forward declarations
// improve compile time. 
class Vector2f;
class Animation;
class AABB;

class Sprite 
{
protected:
    
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture*     texture;

    // Rendering sprite sheet targets
    SDL_Rect* srcRect;
    SDL_Rect* tarRect;
    int offsetX;
    int offsetY;

    // Player properties
   // SDL_Rect targetRectangle;
    float speed;
    Vector2f* position;
    Vector2f* velocity;
    // Animations
    int maxAnimations;
    Animation** animations;

    // bounding box
    AABB *aabb;

public:
    Sprite();
    virtual ~Sprite();

    virtual void init(int maxAnimations, bool linearAnimation, Vector2f* position);

    virtual void draw(SDL_Renderer *renderer);
    
    virtual void update(float timeDeltaInSeconds);

    virtual int getCurrentAnimationState() = 0;

    

    virtual Vector2f* getPosition();
    void setPosition(Vector2f* pos);

    Vector2f* getVelocity();

    void setVelocity(Vector2f* velocity);

    void setOffsetX(int offsetX);
    void setOffsetY(int offsetY);


    SDL_Rect* getSrcRect();
    SDL_Rect* getTarRect();

    // Bounding box
    AABB* getAABB();

};

#endif