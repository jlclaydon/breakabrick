#ifndef ADD_1_BALL_H_
#define ADD_1_BALL_H_

#include "PowerUp.h"
class PowerUp_Add1Ball : public PowerUp
{
public:
	PowerUp_Add1Ball();
	~PowerUp_Add1Ball();

	void performPowerUp();

	void init(int windowWidth, int windowHeight, Player* player, Game* game);
};

#endif
