#ifndef BRICK_H_
#define BRICK_H_

#include "Sprite.h"
class Brick : public Sprite
{
protected:

	int state;
	int red, green, blue, alpha;

	int type;

	bool alive = true;


	int SPRITE_HEIGHT;
	int SPRITE_WIDTH;
	float halfHeight;
	float halfWidth;

public:

	enum BrickStates { NORMAL };

	Brick();

	~Brick();

	

	void init(int type, int width, int height, Vector2f* startPos);

	void takeHit();
	bool isAlive();

	int getCollidedFace(AABB* ballBoundingBox);

	int getCurrentAnimationState();


	void drawColouredBlock(SDL_Renderer* renderer);

	void update(float timeDelta);

	int getType();

};

#endif
