#include "Game.h"

#include "Breakabrick.h"
#include "Brick.h"
#include "Ball.h"
#include "Player.h"
#include "PowerUp.h"
#include "PowerUpManager.h"
#include "TimeUtils.h"
#include "UserInput.h"


void Game::callQuit()
{
    quit = true;
}

Game::Game()
{
}

Game::~Game()
{
    delete backgroundTexture;
    backgroundTexture = nullptr;

    delete powerUpPenaltyTexture;
    powerUpPenaltyTexture = nullptr;

    delete powerUpBonusTexture;
    powerUpBonusTexture = nullptr;
   
    delete player;
    player = nullptr;   

    delete playerTexture;
    playerTexture = nullptr;

    delete ballTexture;
    ballTexture = nullptr;    

    delete[] brickTextures;
    for (int i = 0; i < 6; ++i) {
        brickTextures[i] = nullptr;
    }

    delete[] bricks;
    for (int i = 0; i < COLS; ++i) 
    {
        for (int j = 0; j < ROWS; ++j) 
        {
            bricks[i][j] = nullptr;
        }
    }

    for (int i = 0; i < balls.size(); ++i)
    {
        delete balls[i];
        balls[i] = nullptr;
    }   
    
}

void Game::init(Breakabrick* breakabrick, TimeUtils* timeUtils, UserInput* input, SDL_Renderer* gameRenderer)
{
    // Tell this game class instance about the objects it needs to interact with
    this->breakabrick = breakabrick;
    this->userInput = input;
    this->timeUtils = timeUtils;	
	this->gameRenderer = gameRenderer;

    // This is should probably be private in breakabrick... <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    this->WINDOW_WIDTH = breakabrick->WINDOW_WIDTH;
    this->WINDOW_HEIGHT = breakabrick->WINDOW_HEIGHT;
    CENTER_X = WINDOW_WIDTH / 2;
    CENTER_Y = WINDOW_HEIGHT / 2;

    // Texture setup ==============================================================================

	backgroundTexture = createTextureFromFile("assets/images/stars.png", gameRenderer);
	playerTexture = createTextureFromFile("assets/images/paddle.png", gameRenderer);
	ballTexture = createTextureFromFile("assets/images/ball.png", gameRenderer);
    powerUpBonusTexture = createTextureFromFile("assets/images/pickups.png", gameRenderer);
    powerUpPenaltyTexture = createTextureFromFile("assets/images/badpickups.png", gameRenderer);

    if (backgroundTexture == nullptr) throw std::runtime_error("Background image not found\n");
    if (playerTexture == nullptr) throw std::runtime_error("Player image not found\n");
    if (ballTexture == nullptr) throw std::runtime_error("Ball image not found\n");
    if (powerUpBonusTexture == nullptr) throw std::runtime_error("powerUp bonus image not found\n");
    if (powerUpPenaltyTexture == nullptr) throw std::runtime_error("powerUp penalty image not found\n");

    // Scrolling background setup =================================================================

	int bgTextureWidth, bgTextureHeight;

	SDL_QueryTexture(backgroundTexture, NULL, NULL, &bgTextureWidth, &bgTextureHeight);

	scrollingBackground = new ScrollingBackground();
	scrollingBackground->init(WINDOW_WIDTH, WINDOW_HEIGHT, bgTextureWidth, bgTextureHeight, 30, false, true);

    // Brick texture setup ========================================================================

	for (int i = 0; i < 11; ++i)
	{
		std::string str = "assets/images/";
		str += std::to_string(i);
		str += ".png";

		brickTextures[i] = createTextureFromFile(str.c_str(), gameRenderer);
	}

    // Player and controlls set up ================================================================

    player = new Player();
    player->init(WINDOW_WIDTH, WINDOW_HEIGHT);
    player->setGame(this);
        
    userInput->setPlayer(player);
    userInput->setGame(this);    

    // The balls ==================================================================================
    
    Ball* ball = new Ball();

    ballStartPos = new Vector2f(CENTER_X, CENTER_Y);

    ball->init(ballStartPos, WINDOW_WIDTH); 

    balls.push_back(ball);

    resetBall();


    // The bricks =================================================================================
   
    // Calculate the size of the bricks, the spacing between them and the padding to the screen 
    // edges, according to the size of the game window.

    WIDTH = WINDOW_WIDTH / 16;

    HEIGHT = WIDTH / 4;

    SPACING = 0.2 * WIDTH;

    // The occupied width is the distance in pixels between the left-hand edge of the left-most brick
    //  column and the right-hand edge of the right-most brick column
    int occupiedWidth = (WIDTH * COLS) + ((COLS - 1) * SPACING); 

    int freeWidth = WINDOW_WIDTH - occupiedWidth; // The remaining screen width

    PADDING = freeWidth / 2; // Padding between the left edge of the screen, and the left-most brick column 

    for (int i = 0; i < COLS; ++i)
    {
        for (int j = 0; j < ROWS; ++j)
        {

            bricks[i][j] = new Brick();

            Vector2f brickPos = Vector2f((i * WIDTH) + (i * SPACING) + PADDING,
                                        (j * -HEIGHT) + (j * -SPACING) + CENTER_Y);

            bricks[i][j]->init(j, WIDTH, HEIGHT, &brickPos);
        }

    }

    // Congratulations/gameover message setup =====================================================

    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);   

    primaryFont = TTF_OpenFont("assets/fonts/OpenSans-Regular.ttf", WINDOW_WIDTH / 20);
    if (primaryFont == NULL) cout << "Error loading font:" << TTF_GetError() << endl;

    SDL_Color textCol = { 255, 255, 255 };

    string str = "CONGRATULATIONS!";

    SDL_Surface* textSurface = TTF_RenderText_Solid(primaryFont, str.c_str(), textCol);  

    congratsMessage = SDL_CreateTextureFromSurface(gameRenderer, textSurface);

    str = "Game Over!";

    textSurface = TTF_RenderText_Solid(primaryFont, str.c_str(), textCol);

    gameoverMessage = SDL_CreateTextureFromSurface(gameRenderer, textSurface);

    // Set up PowerUpManager ======================================================================
    powerUpManager = new PowerUpManager();
    powerUpManager->init(WINDOW_WIDTH, WINDOW_HEIGHT, WIDTH, player, this);
}

void Game::loseALife()
{
    if (lives > 0)
    {
        Ball* ball = new Ball();
        ball->init(ballStartPos, WINDOW_WIDTH);

        balls.push_back(ball);

        resetBall();
    }

    --lives;    
    
}

void Game::drawBackground()
{
    SDL_Rect primarySource = SDL_Rect(); // Primary
    SDL_Rect primaryTarget = SDL_Rect(); // Primary
    SDL_Rect secondarySource = SDL_Rect(); // Overflow   
    SDL_Rect secondaryTarget = SDL_Rect(); // Overflow  

    scrollingBackground->assignTargets(&primarySource, &primaryTarget, &secondarySource, &secondaryTarget);

    SDL_RenderCopy(gameRenderer, backgroundTexture, &primarySource, &primaryTarget);
    SDL_RenderCopy(gameRenderer, backgroundTexture, &secondarySource, &secondaryTarget);
}

void Game::draw()
{
    SDL_RenderClear(gameRenderer);

    drawBackground();    

    // Draw player
    SDL_RenderCopy(gameRenderer, playerTexture, player->getSrcRect(), player->getTarRect());

    // Draw balls
    for (int i = 0; i < balls.size(); ++i)
    {
        SDL_RenderCopy(gameRenderer, ballTexture, balls[i]->getSrcRect(), balls[i]->getTarRect());
    }
    
    // Draw power ups
    int activePowerUps = powerUpManager->getPowerUpsLength();

    for (int i = 0; i < activePowerUps; ++i)
    {
        PowerUp* powerUp = powerUpManager->getPowerUp(i);

        if (powerUp->isBonus())
        {
            SDL_RenderCopy(gameRenderer, powerUpBonusTexture, powerUp->getSrcRect(), powerUp->getTarRect());
        } 
        else
        {
            SDL_RenderCopy(gameRenderer, powerUpPenaltyTexture, powerUp->getSrcRect(), powerUp->getTarRect());
        }
    }
    

    int aliveBricks = 0; // Extremely lazy solution to making the congratulations message display

    //draw bricks, and increment aliveBricks counter... ewwwwww
    for (int i = 0; i < COLS; ++i)
    {
        for (int j = 0; j < ROWS; ++j)
        {

            if (bricks[i][j]->isAlive())
            {
                ++aliveBricks;
                SDL_RenderCopy(gameRenderer, brickTextures[bricks[i][j]->getType()], bricks[i][j]->getSrcRect(), bricks[i][j]->getTarRect());
            }
        }
    }

    int w, h;
    SDL_QueryTexture(congratsMessage, NULL, NULL, &w, &h);

    SDL_Rect source = SDL_Rect();
    SDL_Rect target = SDL_Rect();
    target.x = CENTER_X - (w / 2);
    target.y = CENTER_Y - (h / 2);
    target.w = w;
    target.h = h;


    if (!aliveBricks) SDL_RenderCopy(gameRenderer, congratsMessage, NULL, &target); // congratulations

    if (lives < 0) 
    {
        SDL_RenderCopy(gameRenderer, gameoverMessage, NULL, &target); // gameover
    }

    SDL_RenderPresent(gameRenderer);
}

void Game::collisionDetection()
{

    // Check player-paddle against ball(s)
    
    for (int i = 0; i < balls.size(); ++i)
    {
        if (player->getAABB()->intersects(balls[i]->getAABB()))
        {
            int factor = player->getXFactor(balls[i]);

            balls[i]->hitPlayer(factor);
        }
    }

    // Check bricks against balls              
    
    for (int i = 0; i < COLS; ++i)
    {
        for (int j = 0; j < ROWS; ++j)
        {
            for (int k = 0; k < balls.size(); ++k) {

                if (bricks[i][j]->isAlive() && balls[k]->getAABB()->intersects(bricks[i][j]->getAABB()))
                {

                    bricks[i][j]->takeHit(); // Call the brick objects method that removes it from the game

                    int direction = bricks[i][j]->getCollidedFace(balls[k]->getAABB());
                    balls[k]->collideWithBrick(direction);

                    powerUpManager->runLottery(bricks[i][j]->getPosition());
                }
            }
        }
    }
    

    // Check for player-paddle against powerups
    for (int i = 0; i < powerUpManager->getPowerUpsLength(); ++i)
    {
        PowerUp* powerUp = powerUpManager->getPowerUp(i);

        if (powerUp->getAABB()->intersects(player->getAABB()))
        {
            powerUp->performPowerUp();
        }
    }
}

void Game::update(float timeDelta)
{
    player->update(timeDelta);
    scrollingBackground->update(timeDelta);
    powerUpManager->update(timeDelta);

    // update bricks
    for (int i = 0; i < COLS; ++i)
    {
        for (int j = 0; j < ROWS; ++j)
        {
            bricks[i][j]->update(timeDelta);
        }
    }

    // update balls
    for (int i = 0; i < balls.size(); )
    {
        balls[i]->update(timeDelta);

        // if ball has fallen below bottom of the screen, remove it from balls container, then continue
        if (balls[i]->getPosition()->getY() > WINDOW_HEIGHT)
        {
            balls.erase(balls.begin() + i);
            continue;
        }

        // otherwise current ball is valid, so increment 'i', and evaluate next ball.
        ++i;
    }        

    // if balls container empty, lose a life and set up next ball.
    if (!balls.size()) loseALife();

    cout << lives << endl;

    collisionDetection();
}

void Game::resetBall()
{
    for (int i = 0; i < balls.size(); ++i)
    {
        balls[i]->setPosition(new Vector2f(CENTER_X, CENTER_Y + 50));
        balls[i]->setVelocity(new Vector2f(0, 0));
    }
   // ball->setPosition(new Vector2f(CENTER_X, CENTER_Y + 50));
   // ball->setVelocity(new Vector2f(0, 0));
}

void Game::launchBall()
{
    for (int i = 0; i < balls.size(); ++i)
    {
        balls[i]->setVelocity(new Vector2f(0, 600));
    }   
}

void Game::addBalls(int quantity)
{
 

    for (int i = 0; i < quantity; ++i)
    {
        Ball* ball = new Ball();

        Vector2f startPosition = balls[0]->getPosition();
        Vector2f startVelocity = balls[0]->getVelocity();

        startVelocity.setX(startVelocity.getX() * -1);

        ball->init(&startPosition, WINDOW_WIDTH);
        ball->setVelocity(&startVelocity);

        balls.push_back(ball);

        

    }
}

void Game::loadLevel(int level)
{
}

int Game::getCurrentLevelNo()
{
    return 0;
}

void Game::run()
{
    while (!quit) // while quit is not true
    {      

        userInput->processInputs();

        timeUtils->update();
        
        float deltaTime = timeUtils->getTimeDelta();

        // Update Breakabrick Objects
        update(deltaTime);

        //Draw stuff here.
        draw();
        
        SDL_Delay(1);
    }
}