#ifndef LEVEL_H_
#define LEVEL_H_

#include "TextureUtils.h"

class Ball;
class Brick;
class Player;
class TimeUtils;
class UserInput;

class ScrollingBackground;

class Level
{
private:


   
public:

	Level();
	~Level();

	void init();

	void run();
};

#endif
