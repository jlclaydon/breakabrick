#include "MenuButtonLargeText.h"
#include "Vector2f.h"

MenuButtonLargeText::MenuButtonLargeText()
{
	
}

MenuButtonLargeText::~MenuButtonLargeText()
{

}

void MenuButtonLargeText::init(SDL_Renderer* gameRenderer, string buttonText, int xPos, int yPos)
{
	this->gameRenderer = gameRenderer;

	// Setup font
	primaryFont = TTF_OpenFont("assets/fonts/OpenSans-Regular.ttf", 28);
	if (primaryFont == NULL) cout << "Error loading font:" << TTF_GetError() << endl;

	// set font colour
	SDL_Color textCol = { 255, 255, 255 };	

	// create a surface from text given in init() arg #1
	SDL_Surface* textSurface = TTF_RenderText_Solid(primaryFont, buttonText.c_str(), textCol);

	// create texture from textSurface
	textTexture = SDL_CreateTextureFromSurface(gameRenderer, textSurface);

	// Find the dimensions of the newly created text-texture
	int width, height;
	SDL_QueryTexture(textTexture, NULL, NULL, &width, &height);

	// Use dimensions determined above to set SDL_Rect, used as a target to position button, as well as 
	// a target for the background for the button
	targetRect = new SDL_Rect();
	targetRect->x = xPos;
	targetRect->y = yPos;
	targetRect->w = width;
	targetRect->h = height;

	// Give the button a bounding box
	aabb = new AABB(new Vector2f(xPos, yPos), height, width);
}

void MenuButtonLargeText::draw()
{
	// background colour of button
	SDL_SetRenderDrawColor(gameRenderer, 255, 0, 255, 255);
	// button background, simple fill rather than texture fill
	SDL_RenderFillRect(gameRenderer, targetRect);
	// layer the text-texture over the button-texture
	SDL_RenderCopy(gameRenderer, textTexture, NULL, targetRect);
}

AABB* MenuButtonLargeText::getAABB()
{
	return aabb;
}

