/*
* HOOLIGAN ENGINE COPYRIGHT 2021
*/

#ifndef TIME_UTILS_H_
#define TIME_UTILS_H_

#include "SDL2Common.h"
#include <string>

class TimeUtils
{
private:

    unsigned int timeAtBoot;

    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex;
    unsigned int timeDelta;
    float timeDeltaInSeconds;


public:

    TimeUtils();
    ~TimeUtils();

    void update();

    float getTimeDelta();

    unsigned int getCurrentTimeIndex();

    std::string getTimeElapsedString();

};

#endif

