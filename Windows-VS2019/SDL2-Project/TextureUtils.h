#ifndef TEXTURE_UTILS_H_
#define TEXTURE_UTILS_H_

#include "SDL2Common.h"
#include <iostream>

using std::cout;
using std::endl;

SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer);

#endif