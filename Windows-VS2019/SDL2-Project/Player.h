#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;
class Ball;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information

    // Bullet spawn
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    Game* game;

    // Score
    int points;

    int paddleWidthInitial;
    int paddleHeight;
    int paddleCurrentWidth;
   
    int PLAYER_Y;

    int initialWidth;

    const int SPRITE_HEIGHT = 25;
    const int SPRITE_WIDTH = 100;

    static const int PADDLE_INDEX_MAX = 5;
    int allowablePaddleSizes[PADDLE_INDEX_MAX];
    int paddleIndexCurrent;
    

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{NORMAL};
    
    void init(int windowWidth, int windowHeight);

    void setGame(Game* game);
    void fire();

    void setPosition(float x, float y);

    int getCurrentAnimationState();

    //Overloads
    void update(float timeDeltaInSeconds);

    void addScore(int points);
    int getScore();

    int getXFactor(Ball* ball);

    int getPlayerY();

    void increasePaddleSize();
    void decreasePaddleSize();

};



#endif