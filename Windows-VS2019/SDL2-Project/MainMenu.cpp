#include "MainMenu.h"

#include "AABB.h"
#include "Breakabrick.h"
#include "MenuButtonLargeText.h"
#include "ScrollingBackground.h"
#include "TimeUtils.h"
#include "Vector2f.h"

AABB* MainMenu::mouseAABB()
{
    int x, y;

    SDL_GetMouseState(&x, &y);

    AABB* aabb = new AABB();
    aabb->init(new Vector2f(x, y), 2, 2);

    return aabb;
}

MainMenu::MainMenu()
{

}

MainMenu::~MainMenu()
{
}

void MainMenu::init(Breakabrick* breakabrick, SDL_Renderer* gameRenderer, TimeUtils* timeUtils, UserInput* userInput)
{
    this->breakabrick = breakabrick;
    this->gameRenderer = gameRenderer;
    this->timeUtils = timeUtils;
    this->userInput = userInput;

    button = new MenuButtonLargeText();
    button->init(gameRenderer, "This be a button", 100, 100);

    backgroundTexture = createTextureFromFile("assets/images/stars.png", gameRenderer);

    int bgTextureWidth, bgTextureHeight;

    SDL_QueryTexture(backgroundTexture, NULL, NULL, &bgTextureWidth, &bgTextureHeight);

    scrollingBackground = new ScrollingBackground();
    scrollingBackground->init(breakabrick->WINDOW_WIDTH, breakabrick->WINDOW_HEIGHT, bgTextureWidth, bgTextureHeight, 30, false, true);
}

void MainMenu::run()
{
    while (!quit) {

        SDL_Event event;

        if (SDL_PollEvent(&event))  // test for events
        {
            switch (event.type)
            {
            case SDL_QUIT:
                breakabrick->setQuit(true);
                setQuit(true);

                std::cout << "quit called from Breakabrick [X]" << std::endl;
                break;

                // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    setQuit(true);

                    std::cout << "quit called from Breakabrick [esc]" << std::endl;
                    break;
                }
                break;

                // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.      
                    break;
                }
                break;

            default:
                // not an error, there's lots we don't handle. 
                break;
            }
        }

        timeUtils->update();

        float timeDelta = timeUtils->getTimeDelta();

        scrollingBackground->update(timeDelta);

        SDL_RenderClear(gameRenderer);

        drawBackground();

        button->draw();

        if (button->getAABB()->intersects(mouseAABB()))
        {
            switch (event.type)
            {
            case SDL_MOUSEBUTTONDOWN:
                cout << "clicky clicky" << endl;
                break;
            }
        }

        SDL_RenderPresent(gameRenderer);

        // SDL_Delay(1);        

        SDL_GetMouseState(&mouseX, &mouseY);

        string elapsedTime = timeUtils->getTimeElapsedString();
       //   cout << elapsedTime << " | x: " << mouseX << " y: " << mouseY << endl;

        SDL_Delay(1);
    }
}

void MainMenu::setQuit(bool quit)
{
    this->quit = quit;
}

void MainMenu::drawBackground()
{
    SDL_Rect primarySource = SDL_Rect(); // Primary
    SDL_Rect primaryTarget = SDL_Rect(); // Primary
    SDL_Rect secondarySource = SDL_Rect(); // Overflow   
    SDL_Rect secondaryTarget = SDL_Rect(); // Overflow  

    scrollingBackground->assignTargets(&primarySource, &primaryTarget, &secondarySource, &secondaryTarget);

    SDL_RenderCopy(gameRenderer, backgroundTexture, &primarySource, &primaryTarget);
    SDL_RenderCopy(gameRenderer, backgroundTexture, &secondarySource, &secondaryTarget);
}
