#include "ScrollingBackground.h"
#include <iostream>
#include <stdexcept>

void ScrollingBackground::updateRenderingTargets()
{
    if (scanPoint + scanPointMax < spriteMax) // we only need one target
    {
        // all inline boolean ifs vertical scroll : horizontal scroll;
        
        srcRect1->x = verticalScroll ? 0 : scanPoint;
        srcRect1->y = verticalScroll ? scanPoint : 0;
        srcRect1->w = windowWidth;
        srcRect1->h = windowHeight;

        tarRect1->x = 0;
        tarRect1->y = 0;
        tarRect1->w = windowWidth;
        tarRect1->h = windowHeight;

        tarRect2->w = 0;
        tarRect2->h = 0;
    }
    else // we need two targets
    {

        // get whats left of the righthand / bottom side and print it on the left / top
        srcRect1->x = verticalScroll ? 0 : scanPoint;
        srcRect1->y = verticalScroll ? scanPoint : 0;
        srcRect1->w = verticalScroll ? windowWidth : textureWidth - scanPoint;
        srcRect1->h = verticalScroll ? textureHeight - scanPoint : windowHeight;

        tarRect1->x = 0; // always prints to top left of target
        tarRect1->y = 0;
        tarRect1->w = verticalScroll ? windowWidth : textureWidth - scanPoint;
        tarRect1->h = verticalScroll ? textureHeight - scanPoint : windowHeight;
       

        // then print the rest of the screen from the left / top edge of the sheet
        srcRect2->x =  0; //always prints from top left of source
        srcRect2->y =  0;
        srcRect2->w = verticalScroll ? windowWidth : windowWidth - srcRect1->w;
        srcRect2->h = verticalScroll ? windowHeight - srcRect1->h : windowHeight;

        tarRect2->x = verticalScroll ? 0 : srcRect1->w;
        tarRect2->y = verticalScroll ? srcRect1->h : 0;
        tarRect2->w = verticalScroll ? windowWidth : windowWidth - srcRect1->w;
        tarRect2->h = verticalScroll ? windowHeight - srcRect1->h : windowHeight;

    }
}

ScrollingBackground::ScrollingBackground()
{
}

ScrollingBackground::~ScrollingBackground()
{
}

void ScrollingBackground::init(int windowWidth, int windowHeight, int textureWidth, int textureHeight, int scrollSpeed, bool verticalScroll, bool reversed)
{
    this->windowWidth = windowWidth;
    this->windowHeight = windowHeight;
    this->textureWidth = textureWidth;
    this->textureHeight = textureHeight;
    this->scrollSpeed = scrollSpeed;
    this->verticalScroll = verticalScroll;
    this->reversed = reversed;

    scanPoint = 0;

    if (verticalScroll)
    {
        scanPointMax = windowHeight;
        spriteMax = textureHeight;
    }
    else
    {
        scanPointMax = windowWidth;
        spriteMax = textureWidth;
    }    

    srcRect1 = new SDL_Rect();
    tarRect1 = new SDL_Rect();
    srcRect2 = new SDL_Rect();    
    tarRect2 = new SDL_Rect();      

    if (windowHeight > textureHeight || windowWidth > windowWidth)
    {
         throw std::runtime_error("Error - Background sprite is smaller than window\n");
    }
}

void ScrollingBackground::update(float timeDelta)
{
    if (reversed) 
    {
        scanPoint -= timeDelta * scrollSpeed;
        if (scanPoint < 0) scanPoint = spriteMax;
    }
    else
    {
        scanPoint += timeDelta * scrollSpeed;
        if (scanPoint >= spriteMax) scanPoint = 0;
    }
  

	updateRenderingTargets();
}

void ScrollingBackground::assignTargets(SDL_Rect* primarySource, SDL_Rect* primaryTarget, SDL_Rect* secondarySource, SDL_Rect* secondaryTarget)
{
    primarySource->x = srcRect1->x;
    primarySource->y = srcRect1->y;
    primarySource->h = srcRect1->h;
    primarySource->w = srcRect1->w;

    primaryTarget->x = tarRect1->x;
    primaryTarget->y = tarRect1->y;
    primaryTarget->w = tarRect1->w;
    primaryTarget->h = tarRect1->h;

    secondarySource->x = srcRect2->x;
    secondarySource->y = srcRect2->y;
    secondarySource->w = srcRect2->w;
    secondarySource->h = srcRect2->h;

    secondaryTarget->x = tarRect2->x;
    secondaryTarget->y = tarRect2->y;
    secondaryTarget->w = tarRect2->w;
    secondaryTarget->h = tarRect2->h;    
}
