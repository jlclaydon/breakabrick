#include "Ball.h"
#include "Animation.h"
#include "AABB.h"
#include "TextureUtils.h"

Ball::Ball() : Sprite()
{
    state = NORMAL;
}

Ball::~Ball()
{
}

void Ball::init(Vector2f* startPos, int windowWidth)
{
   this->windowWidth = windowWidth;

   this->speed = windowWidth / 10 * 6;
   BALL_W = windowWidth / 100;

    // Call sprite constructor
    Sprite::init(1, true, startPos);   

    // Setup the animation structure
    animations[NORMAL]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);


    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.04f);
    }
      

    aabb = new AABB(this->getPosition(), BALL_W, BALL_W);

    srcRect = new SDL_Rect();
    srcRect->x = 0;
    srcRect->y = 0;
    srcRect->h = SPRITE_HEIGHT;
    srcRect->w = SPRITE_WIDTH;

    tarRect = new SDL_Rect();
    tarRect->x = this->position->getX();
    tarRect->y = this->position->getY();
    tarRect->h = BALL_W;
    tarRect->w = BALL_W;
}

int Ball::getCurrentAnimationState()
{

	return state;
   
}

void Ball::reverseHorizontal()
{
    velocity->setX(velocity->getX() * -1);
}

void Ball::reverseVertical()
{
    velocity->setY(velocity->getY() * -1);
}

void Ball::hitPlayer(int factor)
{
    velocity = new Vector2f(factor * 5, -600);
    velocity->normalise();
    velocity->scale(speed);
}

void Ball::update(float timeDelta)
{   
    

    if (position->getX() > windowWidth - BALL_W) //need to change these hardwired numbers
    {
        int x = windowWidth - BALL_W;
        int y = position->getY();
        position = new Vector2f(x, y);
        reverseHorizontal();
    }

    if (position->getX() < BALL_W)
    {
        int x = BALL_W;
        int y = position->getY();
        position = new Vector2f(x, y);
        reverseHorizontal();
    }  

    if (position->getY() < BALL_W)
    {
        int x = position->getX();
        int y = BALL_W;
        position = new Vector2f(x, y);
        reverseVertical();
    }

    //update sprite after since we want to accelerate away from boundary
    Sprite::update(timeDelta);

    tarRect->x = position->getX();
    tarRect->y = position->getY(); 
}

void Ball::collideWithBrick(int dir)
{
    // NORTH = 0, NORTH_EAST = 1 etc.

    if (dir == NORTH)
    {
        reverseVertical();
        return;
    } 

    if (dir == NORTH_EAST)
    {        

        if (vxIsPositive() && vyIsPositive())
        {
            reverseVertical();
            return;
        }

        if (!vxIsPositive() && vyIsPositive())
        {
            reverseHorizontal();
            reverseVertical();
            return;
        }

        if (vxIsPositive() && !vyIsPositive())
        {
            return;
        }

        if (!vxIsPositive() && !vyIsPositive())
        {
            reverseHorizontal();
            return;
        }
    
    }

    if (dir == EAST)
    {
        reverseHorizontal();
        return;
    }

    if (dir == SOUTH_EAST)
    {
        
        if (vxIsPositive() && vyIsPositive()) //SE
        {         
            return;
        }

        if (!vxIsPositive() && vyIsPositive()) //SW
        {
            reverseHorizontal();           
            return;
        }

        if (vxIsPositive() && !vyIsPositive()) //NE
        {
            reverseVertical();
            return;
        }

        if (!vxIsPositive() && !vyIsPositive()) //NW
        {
            reverseHorizontal();
            reverseVertical();
            return;
        }

    }

    if (dir == SOUTH)
    {
        reverseVertical();
        return;
    }

    if (dir == SOUTH_WEST)
    {

        if (vxIsPositive() && vyIsPositive()) //SE
        {
            reverseHorizontal();
            return;
        }

        if (!vxIsPositive() && vyIsPositive()) //SW
        {
            return;
        }

        if (vxIsPositive() && !vyIsPositive()) //NE
        {
            reverseHorizontal();
            reverseVertical();
            return;
        }

        if (!vxIsPositive() && !vyIsPositive()) //NW
        {         
            reverseVertical();
            return;
        }

    }

    if (dir == WEST)
    {
        reverseHorizontal();
        return;
    }

    if (dir == NORTH_WEST)
    {

        if (vxIsPositive() && vyIsPositive()) //SE
        {
            reverseHorizontal();
            reverseVertical();
            return;
        }

        if (!vxIsPositive() && vyIsPositive()) //SW
        {
            reverseVertical();
            return;
        }

        if (vxIsPositive() && !vyIsPositive()) //NE
        {
            reverseHorizontal();
            return;
        }

        if (!vxIsPositive() && !vyIsPositive()) //NW
        {
            return;
        }

    }
}

bool Ball::vxIsPositive()
{    
    return velocity->getX() > 0;
}

bool Ball::vyIsPositive()
{
    return velocity->getY() > 0;
}

