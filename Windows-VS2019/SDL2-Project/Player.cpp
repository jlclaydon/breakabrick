#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"

#include "AABB.h"
#include "Ball.h"

#include <stdexcept>
#include <string>
#include <iostream>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file
const float Player::COOLDOWN_MAX = 0.2f;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(int windowWidth, int windowHeight)
{
    paddleWidthInitial = windowWidth / 16;
    paddleHeight = paddleWidthInitial / 4;  
    paddleCurrentWidth = paddleWidthInitial;


    PLAYER_Y = windowHeight / 2 + (windowWidth / 5);

    state = NORMAL;
    speed = 50.0f;

   // targetRectangle.w = paddleCurrentWidth;
    //targetRectangle.h = paddleHeight;;

    // Initialise weapon cooldown. 
    cooldownTimer = 0;

    // Init points
    points = 0;

    //mouse control
    bool mouse = true;    

    //postion
    Vector2f position(windowWidth/2, PLAYER_Y);

    // Call sprite constructor
    Sprite::init(1, true, &position);

    // Setup the animation structure
    animations[NORMAL]->init(18, 100, SPRITE_HEIGHT, -1, 0);
  
    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.04f);
    }

    aabb = new AABB(this->getPosition(), paddleHeight, paddleCurrentWidth);

    srcRect = new SDL_Rect();
    srcRect->x = 0;
    srcRect->y = 0;
    srcRect->h = SPRITE_HEIGHT;
    srcRect->w = SPRITE_WIDTH;

    tarRect = new SDL_Rect();
    tarRect->x = this->position->getX();
    tarRect->y = this->position->getY();
    tarRect->h = paddleHeight;
    tarRect->w = paddleCurrentWidth;

    allowablePaddleSizes[0] = paddleWidthInitial / 2;
    allowablePaddleSizes[1] = paddleWidthInitial;
    allowablePaddleSizes[2] = paddleWidthInitial * 1.5;
    allowablePaddleSizes[3] = paddleWidthInitial * 2;
    allowablePaddleSizes[4] = paddleWidthInitial * 2.5;

    paddleIndexCurrent = 1;
}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{

}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;

    tarRect->x = position->getX();
    tarRect->y = position->getY();   

}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::fire()
{   
      
}

void Player::setPosition(float x, float y)
{

    this->position = new Vector2f(x,y);
}

void Player::addScore(int points)
{
    this->points += points;
}

int Player::getScore()
{
    return points;
}

int Player::getXFactor(Ball* ball)
{
    Vector2f* ballV = new Vector2f(ball->getVelocity());

    int ballCenterX = ball->getPosition()->getX() + ball->HALF_BALL_W;
    int playerCenterX = position->getX() + (paddleCurrentWidth / 2);

    int difference = ballCenterX - playerCenterX;

    //std::cout << "Ball X: " << ballCenterX << " | Player X: " << playerCenterX << " | dS: " << difference << " | dS%: " << difference * 100.0 / paddleCurrentWidth  << std::endl;

    return difference * 100.0 / paddleCurrentWidth * 2;
}

int Player::getPlayerY()
{
    return PLAYER_Y;
}

void Player::increasePaddleSize()
{
  
    if (paddleIndexCurrent < PADDLE_INDEX_MAX - 1)
    {
        paddleIndexCurrent += 1;
       
        paddleCurrentWidth = allowablePaddleSizes[paddleIndexCurrent];
        
        aabb = new AABB(this->getPosition(), paddleHeight, paddleCurrentWidth);

        tarRect->w = paddleCurrentWidth;
    }
    
}

void Player::decreasePaddleSize()
{
    if (paddleIndexCurrent > 0)
    {
        paddleIndexCurrent -= 1;

        paddleCurrentWidth = allowablePaddleSizes[paddleIndexCurrent];

        aabb = new AABB(this->getPosition(), paddleHeight, paddleCurrentWidth);

        tarRect->w = paddleCurrentWidth;
    }
}

