#ifndef MENU_BUT_L_TEXT_H_
#define MENU_BUT_L_TEXT_H_

#include "TextureUtils.h"
#include "AABB.h"

#include <string>

using std::string;

class MenuButtonLargeText
{
private:

	string buttonText;
	TTF_Font* font;

	SDL_Renderer* gameRenderer;

	SDL_Texture* textTexture;
	SDL_Texture* buttonTexture;

	AABB* aabb;

	SDL_Rect* targetRect;

	TTF_Font* primaryFont;

public:

	MenuButtonLargeText();
	~MenuButtonLargeText();

	void init(SDL_Renderer* gameRenderer, string buttonText, int xPos, int yPos);	

	void draw();

	AABB* getAABB();

};

#endif

