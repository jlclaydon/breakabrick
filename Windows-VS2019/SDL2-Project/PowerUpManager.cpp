#include "PowerUpManager.h"

#include "Game.h"
#include "PowerUp.h"
#include "PowerUp_Add1Ball.h"
#include "PowerUp_SizeUp.h"
#include "PowerUp_SizeDown.h"
#include "Vector2f.h"
#include <iostream>



PowerUpManager::PowerUpManager()
{	
	
}

PowerUpManager::~PowerUpManager()
{
}

void PowerUpManager::init(int windowWidth, int windowHeight, int brickWidth, Player* player, Game* game)
{
	this->WINDOW_WIDTH = windowWidth;
	this->WINDOW_HEIGHT = windowHeight;
	this->brickWidth = brickWidth;
	powerUpWidth = windowWidth / 48;

	this->player = player;
	this->game = game;
}

void PowerUpManager::runLottery(Vector2f* position)
{
	int randomNumber = rand() % 100 + 1;

	if (randomNumber > PERCENT_CHANCE) return;

	// random number is reassigned, this is very crude! This will likely need to be changed later, since
	// not all powerUps affect the game balance equally. Better powerups should be acquired less often.
	randomNumber = rand() % POWERUP_POOL_SIZE;

	

	switch (randomNumber)
	{
	case SIZEUP:
	 {		// these need curly braces, otherwise VS gives us errors!!!!
		PowerUp_SizeUp* powerUp = new PowerUp_SizeUp();
		powerUp->init(WINDOW_WIDTH, WINDOW_HEIGHT, player, game);

		// Adjust position, according to the brick that spawned it
		Vector2f powerup_offset = Vector2f();
		powerup_offset.zero();
		powerup_offset.setX((brickWidth / 2) - (powerUpWidth / 2));

		// Combine the position given as argument, and the offset
		position->add(&powerup_offset);

		// Set powerUps initial position
		powerUp->setPosition(position);

		powerUps.push_back(powerUp);

		break;
	 }
	case SIZEDOWN:
	 {
		PowerUp_SizeDown* powerUp = new PowerUp_SizeDown();
		powerUp->init(WINDOW_WIDTH, WINDOW_HEIGHT, player, game);

		// Adjust position, according to the brick that spawned it
		Vector2f powerup_offset = Vector2f();
		powerup_offset.zero();
		powerup_offset.setX((brickWidth / 2) - (powerUpWidth / 2));

		// Combine the position given as argument, and the offset
		position->add(&powerup_offset);

		// Set powerUps initial position
		powerUp->setPosition(position);

		powerUps.push_back(powerUp);
		break;
	 }
	case ADD1BALL:
	 {
		PowerUp_Add1Ball* powerUp = new PowerUp_Add1Ball();
		powerUp->init(WINDOW_WIDTH, WINDOW_HEIGHT, player, game);

		// Adjust position, according to the brick that spawned it
		Vector2f powerup_offset = Vector2f();
		powerup_offset.zero();
		powerup_offset.setX((brickWidth / 2) - (powerUpWidth / 2));

		// Combine the position given as argument, and the offset
		position->add(&powerup_offset);

		// Set powerUps initial position
		powerUp->setPosition(position);

		powerUps.push_back(powerUp);

		break;
	 }
	default:
		break;
	}
	

}

void PowerUpManager::update(float deltaTime)
{

	for (int i = 0; i < powerUps.size(); ++i)
	{
		powerUps[i]->update(deltaTime);
	}


	// Remove expired power ups
	for (int i = 0; i < powerUps.size();  )
	{
		// if power up expired, then remove powerUp from vectorList, but DON'T increment i, since the next element
		// to inspect will have the index of THIS iteration!
		if (!powerUps[i]->isValid())
		{
			powerUps.erase(powerUps.begin() + i);
			break;
		} 

		// if power up has passed the bottom of the screen, remove it from the game. Again, dont increment i.
		if (powerUps[i]->getPosition()->getY() > WINDOW_HEIGHT)
		{
			powerUps.erase(powerUps.begin() + i);
			break;
		}

		// power up was not expired, increment i, eval next powerup.
		++i;
	}	

}

int PowerUpManager::getPowerUpsLength()
{
	return powerUps.size();
}

PowerUp* PowerUpManager::getPowerUp(int index)
{
	return powerUps[index];
}

