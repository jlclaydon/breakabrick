#ifndef SCROLLING_BG_H_
#define SCROLLING_BG_H_

#include "TextureUtils.h"

class ScrollingBackground
{
private:
    
    // Rendering targets
    SDL_Rect* srcRect1; // Primary
    SDL_Rect* tarRect1; // Primary
    SDL_Rect* srcRect2; // Overflow
    SDL_Rect* tarRect2; // Overflow  
    
    int windowWidth, windowHeight;
    int textureWidth, textureHeight;
    int scrollSpeed; 
    bool verticalScroll;

    // where on the source texture we are drawing from. This increases/decreases depending on scroll
    // direction.
    float scanPoint;

    // The maximum allowable value of scanPoint, which is dependant on the source texture. If the background scrolls
    // horizontally, then the max value will be equal to the width of the image in pixels, while vertical scrolling
    // would consider the height of the source image.
    float scanPointMax;
    float spriteMax;

    bool reversed = true;

    // called by the update method
    void updateRenderingTargets();

public:

    ScrollingBackground();
    ~ScrollingBackground();

    void init(int windowWidth, int windowHeight, int textureWidth, int textureHeight, int scrollSpeed, bool verticalScroll, bool reversed);

    void update(float timeDelta);

    void assignTargets(SDL_Rect* srcRect1, SDL_Rect* tarRect1, SDL_Rect* srcRect2, SDL_Rect* tarRect2);

};

#endif