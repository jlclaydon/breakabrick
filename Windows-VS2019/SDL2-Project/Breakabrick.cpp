#include "Breakabrick.h"
#include "TextureUtils.h"
#include "Game.h"
#include "MainMenu.h"

#include "Vector2f.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>



//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>
#include <iostream>

using std::cout;
using std::endl;

Breakabrick::Breakabrick()
{
    gameWindow = nullptr;
    gameRenderer = nullptr;    

    quit = false;
}

Breakabrick::~Breakabrick()
{
    //Clean up!
      
   
        
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Breakabrick::init(bool devMode)
{
    this->devMode = devMode;    

    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    WINDOW_WIDTH = DM.w;
    WINDOW_HEIGHT = DM.h;

 //   WINDOW_WIDTH = 800; WINDOW_HEIGHT = 600;
 
    // Set up game window
     gameWindow = SDL_CreateWindow("Breakabrick",    // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }    

    timeUtils = new TimeUtils();

    userInput = new UserInput();
    userInput->setBreakabrick(this);       

    SDL_ShowCursor(SDL_DISABLE);
    //SDL_SetRelativeMouseMode(SDL_TRUE);

    srand(time(0));  

    keyStates = SDL_GetKeyboardState(NULL);

    /////////////////////////////////////////



}

void Breakabrick::runBreakabrickLoop()
{
    while (!quit)
    {
       // startMainMenu();   
        startGame();
    }
}

void Breakabrick::startMainMenu()
{
    mainMenu = new MainMenu();
    mainMenu->init(this, gameRenderer, timeUtils, userInput);

    mainMenu->run();
}

void Breakabrick::startGame()
{
    game = new Game();
    game->init(this, timeUtils, userInput, gameRenderer);

    game->run();
}

void Breakabrick::loadLevel(int level)
{
}

void Breakabrick::setQuit(bool quit)
{
    this->quit = quit;
}
