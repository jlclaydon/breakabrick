#include "Animation.h"
#include "AABB.h"
#include "Game.h"
#include "Player.h"
#include "PowerUp.h"
#include "Vector2f.h"

void PowerUp::init(int windowWidth, int windowHeight, Player* player, Game* game)
{
	this->player = player;
	this->game = game;

	SPRITE_WIDTH = SPRITE_HEIGHT = windowWidth / 48;	

	state = 0;
	
	Vector2f pos = new Vector2f(50, 50);

	Sprite::init(1, true, &pos);

	Sprite::setOffsetX(0);
	Sprite::setOffsetY(0);

	animations[0]->init(6, 100, 100, -1, 0);

	for (int i = 0; i < maxAnimations; i++)
	{
		animations[i]->setMaxFrameTime(0.4f);
	}

	aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);

	srcRect = new SDL_Rect();
	srcRect->x = 0;
	srcRect->y = 0;
	srcRect->h = SPRITE_HEIGHT;
	srcRect->w = SPRITE_WIDTH;

	tarRect = new SDL_Rect();
	tarRect->x = this->position->getX();
	tarRect->y = this->position->getY();
	tarRect->h = SPRITE_HEIGHT;
	tarRect->w = SPRITE_WIDTH;
}

void PowerUp::update(float timeDeltaInSeconds)
{
	Sprite::update(timeDeltaInSeconds);

	tarRect->x = position->getX();
	tarRect->y = position->getY();

	position->add(new Vector2f(0, timeDeltaInSeconds * 400 ));
}

bool PowerUp::isValid()
{
	return valid;
}

bool PowerUp::isBonus()
{
	return bonus;
}

int PowerUp::getCurrentAnimationState()
{
	return state;
}
