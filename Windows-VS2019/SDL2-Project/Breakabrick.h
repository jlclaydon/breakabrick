#ifndef BREAKABRICK_H_
#define BREAKABRICK_H_

#include "SDL2Common.h"
#include "TimeUtils.h"
#include "UserInput.h"


#include <vector>

using std::vector;

class Ball;
class Brick;
class Bullet;
class MainMenu;
class NPC;
class Player;
class TimeUtils;
class UserInput;
class Vector2f;

class PowerUp;

class Breakabrick 
{
public: 
      int WINDOW_WIDTH = 800;
      int WINDOW_HEIGHT = 600;
    

    const Uint8* keyStates;

private:
    bool devMode;

    TimeUtils* timeUtils;

    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;
    MainMenu*       mainMenu;
    UserInput*      userInput;
    Game* game;

   
      
    // Window control 
    bool            quit;  

public:

    // Constructor 
    Breakabrick();
    ~Breakabrick();

    // Methods
    void init(bool devMode = false);
    void runBreakabrickLoop();      

    void startGame();

    void loadLevel(int level);

    void setQuit(bool quit);

    void startMainMenu();
};

#endif