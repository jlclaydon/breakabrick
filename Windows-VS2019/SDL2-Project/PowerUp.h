#ifndef POWER_UP_H_
#define POWER_UP_H_

#include "Sprite.h"

class AABB;
class Game;
class Player;
class Vector2f;

class PowerUp : public Sprite
{
protected:

	bool bonus;

	bool valid = true;

	int state;

	int SPRITE_HEIGHT;
	int SPRITE_WIDTH;

	Player* player;
	Game* game;

public:
	void init(int windowWidth, int windowHeight, Player* player, Game* game);

	void update(float timeDeltaInSeconds);

	bool isValid();

	bool isBonus();

	virtual void performPowerUp() = 0;
	int getCurrentAnimationState();
};
#endif

