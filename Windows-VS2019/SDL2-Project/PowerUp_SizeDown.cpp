#include "PowerUp_SizeDown.h"

#include "Player.h"

PowerUp_SizeDown::PowerUp_SizeDown()
{
}

PowerUp_SizeDown::~PowerUp_SizeDown()
{
}

void PowerUp_SizeDown::performPowerUp()
{
	player->decreasePaddleSize();
	valid = false;
}

void PowerUp_SizeDown::init(int windowWidth, int windowHeight, Player* player, Game* game)
{
	PowerUp::init(windowWidth, windowHeight, player, game);

	bonus = false;
}
