#include "PowerUp_Add1Ball.h"

#include "Game.h"

PowerUp_Add1Ball::PowerUp_Add1Ball()
{
}

PowerUp_Add1Ball::~PowerUp_Add1Ball()
{
}

void PowerUp_Add1Ball::performPowerUp()
{
	valid = false;

	game->addBalls(1);
}

void PowerUp_Add1Ball::init(int windowWidth, int windowHeight, Player* player, Game* game)
{
	PowerUp::init(windowWidth, windowHeight, player, game);

	bonus = true;
}
