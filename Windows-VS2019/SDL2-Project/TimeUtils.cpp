#include "TimeUtils.h"


TimeUtils::TimeUtils()
{
    // Vars for calculating dT since last frame
    currentTimeIndex = 0;
    prevTimeIndex = 0;
    timeDelta = 0;
    timeDeltaInSeconds = 0;

    timeAtBoot = SDL_GetTicks();
}

TimeUtils::~TimeUtils()
{
}

void TimeUtils::update()
{
    //Update time delta
    currentTimeIndex = SDL_GetTicks();
    timeDelta = currentTimeIndex - prevTimeIndex; //change in time in milliseconds
    timeDeltaInSeconds = timeDelta * 0.001f; //convert to change in seconds
    prevTimeIndex = currentTimeIndex; //stores current frame time for next frame


}

float TimeUtils::getTimeDelta()
{
    return timeDeltaInSeconds;
}

unsigned int TimeUtils::getCurrentTimeIndex()
{
    return currentTimeIndex;
}

std::string TimeUtils::getTimeElapsedString()
{
    unsigned int difference = SDL_GetTicks() - timeAtBoot;

    int diffInSeconds = difference / 1000;

    return "Runtime: " + std::to_string(diffInSeconds) + " seconds.";
}


