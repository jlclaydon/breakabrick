/*
  Moving Game into it's own structure / files.  
*/

//For exit()
#include <cstdlib>

//for printf
#include <cstdio>

#include "SDL2Common.h"
#include "Breakabrick.h"

const int SDL_OK = 0;

int main( int argc, char* args[] )
{
    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    if (TTF_Init() != SDL_OK)
    {
        printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());        
    }

    Breakabrick* breakabrick = new Breakabrick();

    breakabrick->init();

    breakabrick->runBreakabrickLoop();
    
    delete breakabrick;

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    exit(0);
}