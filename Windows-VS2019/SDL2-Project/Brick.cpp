#include "Brick.h"
#include "Animation.h"
#include "AABB.h"
#include "Vector2f.h"


Brick::Brick()
{

}


Brick::~Brick()
{
    
}

void Brick::init(int type, int width, int height, Vector2f* startPos)
{
    this->type = type;
    SPRITE_HEIGHT = height;
    SPRITE_WIDTH = width;

    halfWidth = 0.5 * width;
    halfHeight = 0.5 * height;    

    // Call sprite constructor
    Sprite::init(1, false, startPos);

    Sprite::setOffsetX(0);
    Sprite::setOffsetY(0);

    // Setup the animation structure
    animations[NORMAL]->init(16, 100, 25, -1, 0);


    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.1f);
    }

    srcRect = new SDL_Rect();
    srcRect->x = 0;
    srcRect->y = 0;
    srcRect->h = SPRITE_HEIGHT;
    srcRect->w = SPRITE_WIDTH;

    tarRect = new SDL_Rect();
    tarRect->x = position->getX();
    tarRect->y = position->getY();
    tarRect->h = SPRITE_HEIGHT;
    tarRect->w = SPRITE_WIDTH;


    aabb = new AABB(this->getPosition(), height, width);
}

void Brick::takeHit()
{
    alive = false;


}

bool Brick::isAlive()
{
    return alive;
}

int Brick::getCollidedFace(AABB* ballBoundingBox)
{
    int cnr = 1; // corner

    AABB northWest = AABB(new Vector2f(position->getX(), position->getY()), cnr, cnr);
    AABB north = AABB(new Vector2f(position->getX(), position->getY()), 1, SPRITE_WIDTH);
    AABB northEast = AABB(new Vector2f(position->getX() + SPRITE_WIDTH- cnr, position->getY()), cnr, cnr);
    AABB east  = AABB(new Vector2f(position->getX() + SPRITE_WIDTH - 1, position->getY()), SPRITE_HEIGHT, 1);
    AABB southEast = AABB(new Vector2f(position->getX() + SPRITE_WIDTH - cnr, position->getY() + SPRITE_HEIGHT - cnr), cnr, cnr);
    AABB south = AABB(new Vector2f(position->getX(), position->getY() + SPRITE_HEIGHT), 1, SPRITE_WIDTH);
    AABB southWest = AABB(new Vector2f(position->getX(), position->getY()) + SPRITE_HEIGHT - cnr, cnr, cnr);
    AABB west  = AABB(new Vector2f(position->getX(), position->getY()), SPRITE_HEIGHT, 1);

    
    if (northEast.intersects(ballBoundingBox)) {
        return 1;
    }
    if (southEast.intersects(ballBoundingBox)) {
        return 3;
    }
    if (southWest.intersects(ballBoundingBox)) {
        return 5;
    }
    if (northWest.intersects(ballBoundingBox)) {
        return 7;
    }

    if (north.intersects(ballBoundingBox)) {
        return 0;
    }
    if (east.intersects(ballBoundingBox)) {
        return 2;
    }
    if (south.intersects(ballBoundingBox)) {
        return 4;
    }
    if (west.intersects(ballBoundingBox)) {
        return 6;
    }

   

    return -1; 
}

int Brick::getCurrentAnimationState()
{
    return state;	
}

void Brick::drawColouredBlock(SDL_Renderer* renderer)
{
    
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);

    SDL_Rect rect;
    rect.x = position->getX();
    rect.y = position->getY();
    rect.h = SPRITE_HEIGHT;
    rect.w = SPRITE_WIDTH;
    SDL_RenderFillRect(renderer, &rect);
}

void Brick::update(float timeDelta)
{
    Sprite::update(timeDelta);
    

    tarRect->x = position->getX();  
    tarRect->y = position->getY(); 

  //  int n = rand() % 16;
    //srcRect->x = n * 100;
}

int Brick::getType()
{
    return type;
}

