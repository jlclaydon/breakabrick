
#ifndef SIZE_UP_H_
#define SIZE_UP_H_

#include "PowerUp.h"

class PowerUp_SizeUp : public PowerUp
{
protected:

public:
	PowerUp_SizeUp();
	~PowerUp_SizeUp();

	void performPowerUp();

	void init(int windowWidth, int windowHeight, Player* player, Game* game);
};

#endif

