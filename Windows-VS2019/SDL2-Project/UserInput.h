/*
* HOOLIGAN ENGINE COPYRIGHT 2021
*/

#ifndef USERINPUT_H_
#define USERINPUT_H_

#include "SDL2Common.h"
#include "Game.h"
#include "Player.h"

class Game;
class Breakabrick;
class Level;

class UserInput
{
	Game* game;

	Breakabrick* breakabrick;	

	void devMode();
	void mouseInput();
	void menuKeys();
	void playerKeys();

	Player* player;

	const Uint8* keyStates;

public:
	
	UserInput();
	~UserInput();	

	void setPlayer(Player* player);
	
	void setGame(Game* game);

	void setBreakabrick(Breakabrick* breakabrick);	

	void processInputs();

};


#endif // !USERINPUT_H_



