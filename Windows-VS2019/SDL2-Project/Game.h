#ifndef GAME_H_
#define GAME_H_

#include "AABB.h"
#include "TextureUtils.h"
#include "ScrollingBackground.h"

#include <stdexcept>
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

class Ball;
class Breakabrick;
class Brick;
class Player;
class PowerUpManager;
class TimeUtils;
class UserInput;

class PowerUp;

class Game
{
public :

     static const int COLS = 8; // max 13
     static const int  ROWS = 6; // max 11

     int WIDTH;
     int HEIGHT;
     float SPACING;
     int PADDING;
     
     int WINDOW_WIDTH;
     int WINDOW_HEIGHT;
     int CENTER_X;
     int CENTER_Y;

     int BGSPRITE_WIDTH = 2048;

    TTF_Font* primaryFont;

    SDL_Texture* congratsMessage;
    SDL_Texture* gameoverMessage;

    Breakabrick* breakabrick;

    PowerUpManager* powerUpManager;

private:

    bool quit = false;

    TimeUtils* timeUtils;

    SDL_Renderer* gameRenderer;

    UserInput* userInput;

    ScrollingBackground* scrollingBackground;

    // Textures //////////////////

    SDL_Texture* backgroundTexture;    
    SDL_Texture* powerUpBonusTexture;
    SDL_Texture* powerUpPenaltyTexture;
    SDL_Texture* brickTextures[11];
    SDL_Texture* playerTexture;
    SDL_Texture* ballTexture;


    // Declare Player
    Player* player; 
    int lives = 3;

    // Balls
    std::vector<Ball*> balls;

    // Bricks
    Brick* bricks[COLS][ROWS];
    Vector2f* ballStartPos;

    

public:
    Game();
    ~Game();

    void init(Breakabrick* breakabrick, TimeUtils* timeUtils, UserInput* input, SDL_Renderer* gameRenderer);

    void loseALife();

    void drawBackground();

    void draw();

    void collisionDetection();

    void update(float timeDelta);

    void resetBall();

    void launchBall();

    void loadLevel(int level);

    int getCurrentLevelNo();

    void run();

    void callQuit();

    void addBalls(int quantity);

    

};

#endif

