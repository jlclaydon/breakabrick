#include "UserInput.h"
#include "Breakabrick.h"
#include <iostream>

void UserInput::devMode()
{
    if (keyStates[SDL_SCANCODE_UP])
    {
        //game->incLevelNo();
    }
    if (keyStates[SDL_SCANCODE_DOWN])
    {
        // game->decLevelNo();              
    }

    if (keyStates[SDL_SCANCODE_0])
    {
        game->loadLevel(0);
    }

    if (keyStates[SDL_SCANCODE_1])
    {
        game->loadLevel(1);
    }

    if (keyStates[SDL_SCANCODE_2])
    {
        game->loadLevel(2);
    }

    if (keyStates[SDL_SCANCODE_3])
    {
        game->loadLevel(3);
    }

    if (keyStates[SDL_SCANCODE_4])
    {
        game->loadLevel(4);
    }

    if (keyStates[SDL_SCANCODE_5])
    {
        game->loadLevel(5);
    }

    if (keyStates[SDL_SCANCODE_RIGHT] || keyStates[SDL_SCANCODE_LEFT])
    {
        game->loadLevel(game->getCurrentLevelNo());
    }

}

void UserInput::mouseInput()
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    player->setPosition(x, player->getPlayerY());
}

void UserInput::menuKeys()
{
    SDL_Event event;

    if (SDL_PollEvent(&event))  // test for events
    {
        switch (event.type)
        {
        case SDL_QUIT:
            breakabrick->setQuit(true);
            game->callQuit();
            std::cout << "quit called from Breakabrick [X]" << std::endl;
            break;

            // Key pressed event
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:             
                breakabrick->setQuit(true);
                game->callQuit();
                std::cout << "quit called from Breakabrick [esc]" << std::endl;
                break;
            }
            break;

            // Key released event
        case SDL_KEYUP:
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                //  Nothing to do here.      
                break;
            }
            break;

        default:
            // not an error, there's lots we don't handle. 
            break;
        }
    }
}

void UserInput::playerKeys()
{
    
    if (keyStates[SDL_SCANCODE_BACKSPACE]) game->resetBall();        
    if (keyStates[SDL_SCANCODE_RETURN]) game->launchBall();    

  //  if (keyStates[SDL_SCANCODE_B]) game->addBalls(1);        
        
    if (keyStates[SDL_SCANCODE_Q])
    {
        std::cout << "quit called from game" << std::endl;
        game->callQuit();
    }
        
    // arrow/cursor keys
    if (keyStates[SDL_SCANCODE_UP])
    if (keyStates[SDL_SCANCODE_DOWN])
    if (keyStates[SDL_SCANCODE_RIGHT])
    if (keyStates[SDL_SCANCODE_LEFT])
    
    // number keys    
    if (keyStates[SDL_SCANCODE_1])  
    if (keyStates[SDL_SCANCODE_2]);    
}

UserInput::UserInput()
{
    game = nullptr;
    player = nullptr;
    breakabrick = nullptr;
 
    keyStates = SDL_GetKeyboardState(NULL); 
}

UserInput::~UserInput()
{
    breakabrick = nullptr;
}

void UserInput::setPlayer(Player* player)
{
    this->player = player;
}

void UserInput::setGame(Game* game)
{
    this->game = game;
}

void UserInput::setBreakabrick(Breakabrick* breakabrick)
{
    this->breakabrick = breakabrick;
}

void UserInput::processInputs()
{
    menuKeys();
    mouseInput();
    playerKeys();
}


