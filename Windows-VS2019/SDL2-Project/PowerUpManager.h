#ifndef POWER_UP_MANAGER_H_
#define POWER_UP_MANAGER_H_

#include <vector>

class Game;
class Player;
class PowerUp;
class PowerUp_Add1Ball;
class PowerUp_sizeDown;
class PowerUp_SizeUp;
class Vector2f;

class PowerUpManager
{
private:

	Game* game;

	Player* player;

	int PERCENT_CHANCE = 20;
	int POWERUP_POOL_SIZE = 3;

	std::vector<PowerUp*> powerUps;

	int WINDOW_WIDTH, WINDOW_HEIGHT, brickWidth, powerUpWidth;

public:
	enum PowerUpTypes { SIZEUP, SIZEDOWN, ADD1BALL};

	PowerUpManager();
	~PowerUpManager();

	void init(int windowWidth, int windowHeight, int brickWidth, Player* player, Game* game);

	void runLottery(Vector2f* position);

	void update(float deltaTime);

	int getPowerUpsLength();

	PowerUp* getPowerUp(int index);

};

#endif

