#include "PowerUp_SizeUp.h"

#include "Player.h"

PowerUp_SizeUp::PowerUp_SizeUp()
{
}

PowerUp_SizeUp::~PowerUp_SizeUp()
{
}

void PowerUp_SizeUp::performPowerUp()
{
	player->increasePaddleSize();
	valid = false;
}

void PowerUp_SizeUp::init(int windowWidth, int windowHeight, Player* player, Game* game)
{
	PowerUp::init(windowWidth, windowHeight, player, game);

	bonus = true;
}
