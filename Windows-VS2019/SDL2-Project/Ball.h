#ifndef BALL_H_
#define BALL_H_

#include "Sprite.h"
#include "Vector2f.h"

class Ball :
    public Sprite
{
protected:
   
    int windowWidth;
    int state;

public:

    Ball();
    ~Ball();

    enum BallState { NORMAL };
    enum CompassDir { NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST};

    void init(Vector2f* startPos, int windowWidth);

    int getCurrentAnimationState();

    static const int SPRITE_HEIGHT = 256;
    static const int SPRITE_WIDTH = 256;

    void reverseHorizontal();
    void reverseVertical();

    void hitPlayer(int factor);

    void update(float timeDelta);

    float BALL_W;
    float HALF_BALL_W;

    void collideWithBrick(int dir);

    bool vyIsPositive();
    bool vxIsPositive();


};

#endif

